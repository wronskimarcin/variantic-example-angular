import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  id: number = 1;
  layoutEnabled: boolean = true;
  arEnabled: boolean = true;
  snapshotEnabled: boolean = true;
  private sub: any;

  constructor(private route: ActivatedRoute) { }

  layoutToggle() {
    this.layoutEnabled = !this.layoutEnabled;
  }
  arToggle() {
    this.arEnabled = !this.arEnabled;
  }
  snapshotToggle() {
    this.snapshotEnabled = !this.snapshotEnabled;
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe((params: any) => {
        this.id = +params['id'];
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
