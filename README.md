# Variantic Player in Angular (example)

## Step 1
Clone this repository into your environment
```bash
git clone git@bitbucket.org:wronskimarcin/variantic-example-angular.git
```

## Step 2
Change [CUSTOMER_LICENSE_KEY] in ./src/index.html
```html
<script async src="https://api.variantic.com/player/init/en/[CUSTOMER_LICENSE_KEY]"></script>
```

## Step 3
Change IDs of models in ./src/app/app.component.ts to models IDs which exist in your Variantic instance.
```typescript
productIds: string[] = [ '116', '119' ]
```